from django.contrib import admin
from django.core.management import call_command

from dumpdata_sample.models import SampleModel
from dumpdata_sample.settings import BASE_DIR

__author__ = 'Khashayar'
__email__ = 'khashayar@ghamati.com'


class SampleModelAdmin(admin.ModelAdmin):

    def save_model(self, request, obj, form, change):
        super(SampleModelAdmin, self).save_model(request, obj, form, change)
        self.get_dumpdata()

    def get_dumpdata(self):
        file_path = f'{BASE_DIR}/fixtures/dumpdata_sample.json'
        f = open(file_path, 'w')
        call_command(
            'dumpdata',
            'dumpdata_sample',  # it's my application name which I defined it in my settings file
            stdout=f,
            format='json',
            indent=3
        )
        f.close()


admin.site.register(SampleModel, SampleModelAdmin)
