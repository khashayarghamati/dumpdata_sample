from django.db import models

__author__ = 'Khashayar'
__email__ = 'khashayar@ghamati.com'


class SampleModel(models.Model):

    name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = "Sample"
        verbose_name_plural = "Samples"
